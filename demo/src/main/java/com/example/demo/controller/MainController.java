package com.example.demo.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;

@RestController
public class MainController {

	@Autowired
	private EmployeesRepo er;
	
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		Employees employees = new Employees();
		employees = er.findByEmpID(empId);
		return employees;
	}
	
	@GetMapping("/getempall")
	public List<Employees> getEmpAll(){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}
	
	@GetMapping("/getempposid")
	public List<Employees> getEmpPosId(@RequestParam Integer posID){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posID);
		return empls;
	}
	
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		
		return respMap;
	}
	
	@PostMapping("/delete")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			er.delete(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		
		return respMap;
	}
	
}
